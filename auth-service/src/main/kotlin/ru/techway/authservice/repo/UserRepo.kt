package ru.techway.authservice.repo

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.techway.authservice.model.User
import java.util.*

@Repository
interface UserRepo : JpaRepository<User, UUID> {

    fun findByName(name: String): User?
}
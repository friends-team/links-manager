package ru.techway.authservice.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType.IDENTITY
import javax.persistence.Id

@Entity
data class User(
        @Id
        @GeneratedValue(strategy = IDENTITY)
        val id: Long,
        val name: String,
        val password: String,
        val admin: Boolean
)
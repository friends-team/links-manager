package ru.techway.authservice.api

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal

@RestController
@RequestMapping("/auth")
class AuthController {

    @GetMapping("/user")
    fun getPrincipal(principal: Principal?): Principal? {
        return principal
    }
}